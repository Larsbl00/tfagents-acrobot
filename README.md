# Tensorflow Acrobot V-1
This project was created to get more familiar with RL.
For the RL process the [TF Agents](https://www.tensorflow.org/agents) library was used.

## Results
The purpose of the environment is to reach the line (visible in the video).
After training for ~110 iterations, the agent has passed the treshold value.
The result is as follows (the video will show 5 runs of the trained network):

![Video of the trained network](videos/PostTraining.mp4)


[Tensorboard](https://www.tensorflow.org/tensorboard/) has been used to visualize the data of the training process.
This is stored in [summary](summary).

![Metric value during training](img/metrics.png)

### Timelapse

![Training Timelapse](videos/Timelapse.mp4)

## Code
All code used to train the `Acrobot-v1` can be found in this [notebook].
This notebook describes the flow from installing the dependencies, to configuring the hyperparams, to training, to loading the training data.
As such, this is the only source required for learning

[notebook]: tensorflow-acrobot.ipynb "tensorflow-acrobot.ipynb"